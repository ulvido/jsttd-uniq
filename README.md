![ECZADO](https://drive.google.com/uc?export=download&id=1QqlZRw6k49bBLkL1XcZpFjLFR8R5YlF_)

# Bu bir deneme uygulamasıdır.
Gitlab ve npmjs 'ye paket yükleme denemesidir. Lütfen görmezden gelin.

[![NPM](https://nodei.co/npm/jsttd-uniq.png)](https://nodei.co/npm/jsttd-uniq/)

## Kurulum
### Local
```
$ npm i -P jsttd-uniq
```
### Global
```
$ npm i -g jsttd-uniq
```

## İlerleme

- [x] git init yap
- [x] CLI özellikleri ekle
- [ ] paketi tamamla :sparkles: :camel: :boom:

## Kullanım
```javascript
const { selamver, topla } = require("jsttd-uniq");

# selam vermek için
console.log(selamver());

# toplamak için
console.log("Topla fonksiyonu 3 + 5 = ", topla(3, 5));
```

## CLI Kullanımı

### LOCAL kurulu ise:
#### sorusor
```
$ ./node_modules/.bin/eczado --sorusor true
```
#### selamla
```
$ ./node_modules/.bin/eczado --isim Ulvi
```
### GLOBAL kurulu ise:
#### sorusor
```
$ eczado --sorusor true
```
#### selamla
```
$ eczado --isim Ulvi
```

## Eslint

#### Lint hatalarını göster:
```
$ ./node_modules/.bin/eslint .
```

#### Belirli bir dosyadaki lint hatalarını göster:
```
$ ./node_modules/.bin/eslint ./lib/eczado.js
```

#### Lint hatalarını tamir et:
```
$ ./node_modules/.bin/eslint . --fix
```

## Test
```
$ npm test
```