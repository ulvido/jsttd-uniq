# Changelog
[Paketin] değişim detayları aşağıda belirtilmiştir.

## [1.0.27] - 2018.11.3
### Tamir Edildi
- version.

## [1.0.26] - 2018.11.3
### Eklendi
- devDependencies --save-exact yapıldı.

## [1.0.25] - 2018.11.3
### Eklendi
- CLI "eczado komutu.
- "eczado --sorusor true" : soru sorar.
- "eczado --isim Ulvi" : Ulvi 'yi selamlar.

## [1.0.24] - 2018.11.2
### Eklendi
- readme ECZADO logo

## [1.0.23] - 2018.11.2
### Eklendi
- eslint kütüphanesi.
### Değişti
- eslint yapılan dosyalar.

## [1.0.20] - 2018.11.1
### Eklendi
- git init yapıldı.
- package.json 'a "engines : node >= 8.12.0" bölümü.
- package.json 'a "repository" bölümü.
- package.json 'a keywords bölümü.
### Değişti
- ~~module export ES6'ya uygun şekle getirildi.~~

## [1.0.19] - 2018.10.31
### Tamir Edildi
- Paketi bulamama sorunu.
### Eklendi
- Paket silinirken ECZADO.txt'yi de sil.

## [1.0.18] - 2018.10.30
### Eklendi
- Proje ana klasöründe ECZADO.txt oluştur.

[Paketin]: https://gitlab.com/ulvido/jsttd-uniq
[1.0.27]: https://www.npmjs.com/package/jsttd-uniq/v/1.0.27
[1.0.26]: https://www.npmjs.com/package/jsttd-uniq/v/1.0.26
[1.0.25]: https://www.npmjs.com/package/jsttd-uniq/v/1.0.25
[1.0.24]: https://www.npmjs.com/package/jsttd-uniq/v/1.0.24
[1.0.23]: https://www.npmjs.com/package/jsttd-uniq/v/1.0.23
[1.0.20]: https://www.npmjs.com/package/jsttd-uniq/v/1.0.20
[1.0.19]: https://www.npmjs.com/package/jsttd-uniq/v/1.0.19
[1.0.18]: https://www.npmjs.com/package/jsttd-uniq/v/1.0.18

