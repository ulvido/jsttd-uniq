#!/usr/bin/env node
"use strict";

const eczado = require("../lib/eczado");
const { sorusor } = require("../lib/sorusor");

const getArguman = (arg) => {
	let argIndex = process.argv.indexOf(arg);
	return (argIndex === -1) ? null : process.argv[argIndex + 1];
};

if (getArguman("--sorusor")) {
	sorusor();
}

if (getArguman("--isim")) {
	const isim = getArguman("--isim");

	let IsmiSelamla = isim ? eczado.selamver(isim) : "Hatalı argüman";

	console.log(IsmiSelamla);
}