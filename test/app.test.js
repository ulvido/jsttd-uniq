const assert = require("chai").assert;
const app = require("../lib/eczado");

describe("App", () => {

	describe("Selam Ver", () => {
		let result = app.selamver("Osman");
		it("Selamver verilen ismi selamlamalı", () => {
			assert.equal(result, "Selam, Osman");
		});
		it("Selamver boş olursa da selam vermeli", () => {
			let result = app.selamver();
			assert.equal(result, "Selam, eyy ismini vermek istemeyen izleyici!");
		});
		it("Selamver string döndürmeli", () => {
			assert.typeOf(result, "string");
		});
		it("Selamver rakam olamaz", () => {
			assert.notTypeOf(result, "number");
		});
	});

	describe("Topla", () => {
		let result = app.topla(1, 2);
		it("Toplama sonucu doğru olmalı", () => {
			assert.equal(result, 3);
		});
		it("toplama sonucu rakam olmalı", () => {
			assert.typeOf(result, "number");
		});
		it("toplama sonucu 0 'dan büyük olmalı", () => {
			assert.isAbove(result, 0);
		});
	});
});

