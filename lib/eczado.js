"use strict";

const selamver = (isim = "eyy ismini vermek istemeyen izleyici!") => {
	return `Selam, ${isim}`;
};

const topla = (rakam1, rakam2) => {
	return rakam1 + rakam2;
};

module.exports = {
	selamver: selamver,
	topla: topla
};