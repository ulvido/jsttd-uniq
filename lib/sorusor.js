"use strict";

const soru = [
	"Naber",
	"Nettin",
	"Nördün"
];

const cevap = [];

const sor = (soruIndex) => {
	process.stdout.write(`\n${soru[soruIndex]}? > `);
};

const sorusor = () => {

	process.stdout.write(`\nAşağıdaki sorulara cevap ver!!!!1!!!1!:\n\n`);



	process.stdin.on("data", data => {
		cevap.push(data.toString().trim());

		if (cevap.length < soru.length) {
			sor(cevap.length);
		} else {
			process.exit();
		}
	});

	process.on("exit", () => {
		process.stdout.write(`\n\n\nCevaplarınız:\n\n`);
		process.stdout.write(`${cevap[0]}\n`);
		process.stdout.write(`${cevap[1]}\n`);
		process.stdout.write(`${cevap[2]}\n`);
		process.stdout.write(`\n\n\n`);

	});

	sor(0);
};

module.exports = {
	sorusor: sorusor
};